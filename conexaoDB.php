<?php

Class Conexao {


	private $host   = 'localhost';
	private $dbname = 'akn_teste';
	private $user   = 'root';
	private $pass   = '';


	public function conectar(){

		try{

			$conexao = new PDO(
				"mysql:host=$this->host;dbname=$this->dbname",
				"$this->user",
				"$this->pass"
			);

			return $conexao;

		} catch(PDOException $e){
			//metodos getCode e getMessage são metodos publicos da instancia PDO.
			echo '<p> ERRO:' . $e-> getCode() . ' Mensagem: ' . $e->getMessage() . '</p>';
		}
	}
}	

?>