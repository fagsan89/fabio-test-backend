<?php

require 'conexaoDB.php';
//instancio objeto
$conexao  = new Conexao();
$con = $conexao->conectar();


$recebedados = include("lista-de-compras.php");
$palavras_erradas   = array("Papel Hignico", "Brocolis", "Chocolate ao leit", "Sabao em po");


//Ordeno os dados antes de enviar para o array listaCompras
 uksort($recebedados, "ordenandoMeses");

 function ordenandoMeses($a, $b ) {

    $mesesOrd = array( 
        'janeiro', 
        'fevereiro', 
        'marco', 
        'abril', 
        'maio', 
        'junho', 
        'julho', 
        'agosto', 
        'setembro', 
        'outubro', 
        'novembro', 
        'dezembro' );

    // Localizo e comparo os meses 
    if ( array_search( $a, $mesesOrd) == array_search( $b, $mesesOrd) ) return 0;
    return array_search( $a, $mesesOrd) > array_search( $b, $mesesOrd) ? 1 : -1;
}


//Caso necessite baixar o arquivo
//incluir o cabeçalho abaixo
//header('Content-Type: text/csv; charset=utf-8');
//header('Content-Disposition: attachment; filename=lista_compras.csv');

$saida = fopen('lista_compras.csv', 'w');

fputcsv($saida, array('Mes', 'Categoria', 'Produto', 'Quantidade'),";");

//percorre e ordena a lista recebida
foreach ($recebedados as $mes => $categorias) {
      ksort($categorias);

    foreach ($categorias as $categoria => $produtos){
      arsort($produtos);
      
            foreach ($produtos as $produto => $quantidade){
                
                $listaCompras[]= array($mes,$categoria,$produto,$quantidade);
                               
        }
    }    
}

// percorre listacompras para trocar as palavras erradas
foreach ($listaCompras as $row)
{
    
    //trato os dados errados
    if ( in_array($row[2],$palavras_erradas)) {

        switch ($row[2]) {
            case 'Papel Hignico':
                    $row[2] = "Papel Higiênico";
                break;
        case 'Brocolis':
                    $row[2] = "Brócolis";
                break;  
        case 'Chocolate ao lei':
                    $row[2] = "Chocolate ao Leite";
                break;   
        case 'Sabao em po':
                    $row[2] = "Sabão em pó";
                break;                       
            default:
                # code...
                break;
        }
     }     


    fputcsv($saida,$row,";");
}

// fechar arquivo
fclose($saida);





//Lendo arquivo para gravar no Banco
$csv_gerado  = fopen("lista_compras.csv", "r");
$linha   = fgetcsv($csv_gerado, 1000, ";");
 
    while  ($linha  = fgetcsv($csv_gerado, 1000, ";"))  {
 
        //var_dump($linha[0]);
        //var_dump($linha[1]);
        //var_dump($linha[2]);
        //var_dump($linha[3]);
        

       $produto_corr = utf8_decode($linha[2]);

       $execDb = $con ->exec("insert into akn_listacompras (AKN_MES,AKN_CATEGORIA,AKN_PRODUTO,AKN_QUANTIDADE) values ('".$linha[0]."','".$linha[1]."','".$produto_corr."','".$linha[3]."')");

    }



//Gero novo arquivo com as correções 

$new_csv = fopen('lista_compras.csv', 'w');
fputcsv($new_csv, array('Mes', 'Categoria', 'Produto', 'Quantidade'),";");
$consultar = $con->prepare(" select AKN_MES,AKN_CATEGORIA,AKN_PRODUTO,AKN_QUANTIDADE from akn_listacompras ");  
$consultar->execute(); 
while ($sqlListar =  $consultar->fetch(PDO::FETCH_ASSOC)){        

        fputcsv($new_csv,$sqlListar,";");
    
    }


// fecha Arquivo
fclose($new_csv);



 
?>

<!DOCTYPE html>
<html>
<head>
    <title>Lista de Compras</title>
</head>
<body >
    <div>
       
        <?if ($execDb) { ?>
            
            <p>Arquivo gerado com sucesso!!</p>
            <p>Dados gravados com sucesso!!</p>
         <?}else{?>
             <p>Arquivo NÃO gerado!!</p>
         <?}?>   

        <button><a href="index.php" style="text-decoration: none;">Voltar</a> </button>
    </div>
</body>
</html>
<script>


</script>

